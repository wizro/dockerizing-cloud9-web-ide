FROM node:12

WORKDIR /usr/src/app

COPY install_cloud9.sh .

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN git clone https://github.com/c9/core.git csdk9v3a

CMD ["/bin/sh", "./instll_cloud9.sh"]

EXPOSE 8080

# Dockerizing Cloud9 Web Ide

This a project on dockerizing [cloud9 ide](https://cloud9-sdk.readme.io/blog). Cloud9 ide is a web based ide it can be run locally or remotely.

## Requirements

- docker engine
- git

## Installation

clone this project

run the following commands to build and run cloud9 ide

`docker volume create --name cloud9`

`docker build -t cloud9ide:1 .`

`docker run -p 5000:8080 --dns=8.8.8.8 --name=cloud9 -v cloud9:/home cloud9ide:1`

go to localhost:5000 on your browser
